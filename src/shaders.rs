pub mod vertex {
    #[derive(VulkanoShader)]
    #[ty = "vertex"]
    #[src = "
        #version 450

        struct Ball{
            ivec3 pos;
            int rad;
            ivec3 vel;
            int den;
            ivec3 acc;
        };

        layout(set = 0, binding = 0) buffer Balls {
            Ball balls[];
        } buf;

        layout(location = 0) in int rad;

        layout(location = 0) flat out int vs_rad;
        layout(location = 1) flat out vec3 color;

        void main() {
            vec3 rel_pos = buf.balls[0].pos+buf.balls[0].vel*0;
            //rel_pos = vec3(0);
            color = normalize(buf.balls[gl_VertexIndex].vel)/2+vec3(0.5);
            gl_Position = vec4(buf.balls[gl_VertexIndex].pos-rel_pos, 1);
            vs_rad = buf.balls[gl_VertexIndex].rad;
        }
    "]
    struct Dummy;
}

pub mod geometry {
    #[derive(VulkanoShader)]
    #[ty = "geometry"]
    #[src = "
        #version 450

        layout(set = 0, binding = 1) buffer State {
            float zoom;
            uint max;
        } state;

        layout(points) in;
        layout(triangle_strip, max_vertices = 4) out;
        
        layout(location = 0) flat in int vs_rad[1];
        layout(location = 1) flat in vec3 color[1];

        layout(location = 0) out vec2 center;
        layout(location = 1) out vec2 pos;
        layout(location = 2) flat out float gs_rad;
        layout(location = 3) flat out vec3 color_end;
        
        void main() {
            vec4 p = gl_in[0].gl_Position;
            vec2 c = vec2(p.xy/p.w);
            float rad = float(vs_rad[0])/p.w;
            float scale = state.zoom;

            center = c;
            pos = c + rad*vec2(1, 1);
            gs_rad = rad;
            gl_Position = vec4(pos, 0, scale);
            color_end = color[0];
            EmitVertex();
            center = c;
            pos = c + rad*vec2(1, -1);
            gs_rad = rad;
            gl_Position = vec4(pos, 0, scale);
            color_end = color[0];
            EmitVertex();
            center = c;
            pos = c + rad*vec2(-1, 1);
            gs_rad = rad;
            gl_Position = vec4(pos, 0, scale);
            color_end = color[0];
            EmitVertex();
            center = c;
            pos = c + rad*vec2(-1, -1);
            gs_rad = rad;
            gl_Position = vec4(pos, 0, scale);
            color_end = color[0];
            EmitVertex();
        }
    "]
    struct Dummy;
}

pub mod fragment {
    #[derive(VulkanoShader)]
    #[ty = "fragment"]
    #[src = "
#version 450

        layout(location = 0) in vec2 center;
        layout(location = 1) in vec2 pos;
        layout(location = 2) flat in float gs_rad;
        layout(location = 3) flat in vec3 color_end;

        layout(location = 0) out vec4 color;

        void main() {
            float fac = distance(pos, center)/gs_rad;
            if(1<fac)
                discard;
            color = vec4(color_end, 1);
        }

    "]
    struct Dummy;
}

pub mod pos_vel {
    #[derive(VulkanoShader)]
    #[ty = "compute"]
    #[src = "
#version 450

layout(local_size_x = 16, local_size_y = 1, local_size_z = 1) in;

struct Ball{
    ivec3 pos;
    int rad;
    ivec3 vel;
    int den;
    ivec3 acc;
};

layout(set = 0, binding = 0) buffer Balls {
    Ball balls[];
} buf;

layout(set = 0, binding = 1) buffer Inputs {
    ivec2 dir;
    int jump;
} inp;

layout(set = 0, binding = 2) buffer State {
    float zoom;
    uint max;
} state;


void main() {
    uint idx = gl_GlobalInvocationID.x;
    if(idx==0) {
        atomicAdd(buf.balls[0].acc.x, 256*inp.dir.x);
        atomicAdd(buf.balls[0].acc.y, 256*inp.dir.y);
        
        atomicAdd(buf.balls[0].rad, -sign(inp.jump-buf.balls[0].rad)*(min(-1, -abs(inp.jump-buf.balls[0].rad)/4)));
    }
    if(idx<state.max) {
        atomicAdd(buf.balls[idx].vel.x, buf.balls[idx].acc.x);
        atomicAdd(buf.balls[idx].vel.y, buf.balls[idx].acc.y);
        atomicAdd(buf.balls[idx].vel.z, buf.balls[idx].acc.z);

        atomicAdd(buf.balls[idx].pos.x, buf.balls[idx].vel.x+buf.balls[idx].acc.x);
        atomicAdd(buf.balls[idx].pos.y, buf.balls[idx].vel.y+buf.balls[idx].acc.y);
        atomicAdd(buf.balls[idx].pos.z, buf.balls[idx].vel.z+buf.balls[idx].acc.z);
        
        buf.balls[idx].acc = ivec3(0);
}   }
 
    "]
    struct Dummy;
}

pub mod collide {
    #[derive(VulkanoShader)]
    #[ty = "compute"]
    #[src = "
#version 450

layout(local_size_x = 16, local_size_y = 16, local_size_z = 1) in;

struct Ball{
    ivec3 pos;
    int rad;
    ivec3 vel;
    int den;
    ivec3 acc;
};

layout(set = 0, binding = 0) buffer Balls {
    Ball balls[];
} buf;

layout(set = 0, binding = 1) buffer State {
    float zoom;
    uint max;
} state;

void main() {
    uint idx = gl_GlobalInvocationID.x;
    uint idy = gl_GlobalInvocationID.y;
    if(idx<=idy) return;

    if(idx<state.max&&idy<state.max) {
        Ball b0 = buf.balls[idx];
        Ball b1 = buf.balls[idy];
        ivec3 disvec = b0.pos - b1.pos;
        float dis = length(disvec);
        float m0 = b0.rad*b0.rad*b0.den;
        float m1 = b1.rad*b1.rad*b1.den;
        int rad = b0.rad + b1.rad;
        vec3 dir = disvec/dis;
        vec3 accvec = vec3(0);
        if(dis<rad) {
            accvec = (rad-dis)/(m0+m1)*dir/16;
        } else {
            float gfac = -pow(2, 8);
            accvec = gfac*dir*(1/(dis*dis)-(rad*rad)/(dis*dis*dis*dis));
        }
        ivec3 accvec0 = ivec3(accvec*m1);
        ivec3 accvec1 = -ivec3(accvec*m0);
        
        atomicAdd(buf.balls[idx].acc.x, accvec0.x);
        atomicAdd(buf.balls[idx].acc.y, accvec0.y);
        atomicAdd(buf.balls[idx].acc.z, accvec0.z);

        atomicAdd(buf.balls[idy].acc.x, accvec1.x);
        atomicAdd(buf.balls[idy].acc.y, accvec1.y);
        atomicAdd(buf.balls[idy].acc.z, accvec1.z);

/*
        atomicAdd(buf.balls[idx].pos.x, accvec0.x);
        atomicAdd(buf.balls[idx].pos.y, accvec0.y);
        atomicAdd(buf.balls[idx].pos.z, accvec0.z);

        atomicAdd(buf.balls[idy].pos.x, accvec1.x);
        atomicAdd(buf.balls[idy].pos.y, accvec1.y);
        atomicAdd(buf.balls[idy].pos.z, accvec1.z);
*/
    }
}
    "]
    struct Dummy;
}

