#[macro_use]
extern crate vulkano;
extern crate vulkano_win;
extern crate winit;
#[macro_use]
extern crate vulkano_shader_derive;
extern crate cgmath;

use std::mem;

use cgmath::Vector3;

use std::sync::Arc;

mod shaders;


#[derive(Copy, Clone)]
#[repr(C)]
struct Ball<T>{
    pos: [T;3],
    rad: T,
    vel: [T;3],
    den: T,
    acc: [T;3],
    buf: T
}

type iBall = Ball<i32>;

impl_vertex!(iBall, rad);


#[repr(C)]
struct Input {
    hor: i32,
    ver: i32,
    
    jump: i32,
    rest: i32,
}

#[repr(C)]
struct State {
    zoom: f32,
    bsize: u32,
}




fn main() {
    use vulkano::instance::{
        Instance,
        PhysicalDevice
    };
    
    let instance = Instance::new(None, &vulkano_win::required_extensions(), None)
        .expect("Instance creation failed");

    let physical = PhysicalDevice::enumerate(&instance).next().expect("No device exists");

    /*
    for family in physical.queue_families() {
        println!("Found queue family with {:?} queue", family.queues_count())
    }
    */
    use winit::{
        EventsLoop,
        WindowBuilder
    };
    use vulkano_win::{
        VkSurfaceBuild
    };
    
    let mut events_loop = EventsLoop::new();
    
    use winit::NativeMonitorId::Numeric;
    let surface = WindowBuilder::new()
        //.with_fulscreen(Numeric(2))
        .with_dimensions(1280, 768)
        .build_vk_surface(&events_loop, instance.clone()).unwrap();
    
    let queue_family = physical.queue_families()
        .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
        .expect("No queue family supported");


    use vulkano::device::{
        Device,
        DeviceExtensions
    };
    let (device, mut queues) = {
        let ext = DeviceExtensions {
            khr_swapchain: true,
            .. DeviceExtensions::none()
        };
        Device::new(physical, physical.supported_features(), &ext,
                    [(queue_family, 0.5)].iter().cloned()).expect("Device creation failed")
    };

    let queue = queues.next().unwrap();
    
    let mut dimensions;
    
    use vulkano::swapchain::{
        Swapchain,
        SurfaceTransform,
        PresentMode,
        acquire_next_image,
        AcquireError,
        SwapchainCreationError
        
    };
    
    let (mut swapchain, mut images) = {
        let caps = surface.capabilities(physical)
            .expect("Getting surface capabilities failed");
        dimensions = caps.current_extent.unwrap();
        let alpha = caps.supported_composite_alpha.iter().next().unwrap();
        let format = caps.supported_formats[0].0;

        Swapchain::new(device.clone(), surface.clone(), caps.min_image_count, format,
                       dimensions, 1, caps.supported_usage_flags, &queue,
                       SurfaceTransform::Identity, alpha, PresentMode::Fifo, true,
                       None).expect("Swapchain creation failed")
    };

    let data = (0 .. 32).map(|p| {
                                 let fac = (p%2+1);
                                 let rad = fac*2048;
                                 let den = fac*fac;
                                 Ball{
                                    pos: Vector3::new(2*2048*p-32*2048, 2*65536*(p%2)-32*2048, 0).into(),
                                    vel: Vector3::new(0, (-(p%2)*16+8)*128/fac, 0).into(),
                                    rad,
                                    den,
                                    acc: [0, 0, 0],
                                    buf: 0
                                }});
    
    //let balls: Vec<_> = data.collect();

    use vulkano::buffer::{
        BufferUsage,
        CpuAccessibleBuffer
    };
    
            
    let bsize = 32;


    let balls = CpuAccessibleBuffer::from_iter(device.clone(), BufferUsage::all(), data)
        .expect("Buffer creation failed");
    
    let data = Input {hor: 0, ver: 0, jump: 2048, rest: 0};

    let input = CpuAccessibleBuffer::from_data(device.clone(), BufferUsage::all(), data)
        .expect("Buffer creation failed");

    let data = State {zoom: 65536f32, bsize};

    let state = CpuAccessibleBuffer::from_data(device.clone(), BufferUsage::all(), data)
        .expect("Buffer creation failed");


    use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;



    let pos_vel = shaders::pos_vel::Shader::load(device.clone())
        .expect("Loading shader failed");

    use vulkano::pipeline::ComputePipeline;

    let pos_vel_pipe = Arc::new(ComputePipeline::new(device.clone(), &pos_vel.main_entry_point(), &())
            .expect("Compute pipline creation failed"));

    let pos_vel_set = Arc::new(PersistentDescriptorSet::start(pos_vel_pipe.clone(), 0)
        .add_buffer(balls.clone()).unwrap()
        .add_buffer(input.clone()).unwrap()
        .add_buffer(state.clone()).unwrap()
        .build().unwrap());


    let collide = shaders::collide::Shader::load(device.clone())
        .expect("Loading shader failed");

    let collide_pipe = Arc::new(ComputePipeline::new(device.clone(), &collide.main_entry_point(), &())
        .expect("failed to create compute pipeline"));

    let collide_set = Arc::new(PersistentDescriptorSet::start(collide_pipe.clone(), 0)
        .add_buffer(balls.clone()).unwrap()
        .add_buffer(state.clone()).unwrap()
        .build().unwrap());



    let render_pass = Arc::new(single_pass_renderpass!(
        device.clone(),
        attachments: {
            color: {
                load: Clear,
                store: Store,
                format: swapchain.format(),
                samples: 1,
            }
        },
        pass: {
            color: [color],
            depth_stencil: {}
        }
    ).unwrap());

    let vs = shaders::vertex::Shader::load(device.clone())
        .expect("Loading shader failed");
    let gs = shaders::geometry::Shader::load(device.clone())
        .expect("Loading shader failed");
    let fs = shaders::fragment::Shader::load(device.clone())
        .expect("Loading shader failed");


    /*
    use vulkano::pipeline::vertex::{
        VertexSource,
        BufferlessDefinition,
        BufferlessVertices
    };// */

    use vulkano::pipeline::GraphicsPipeline;
    use vulkano::framebuffer::Subpass;
    
    let (mut left, mut up, mut right, mut down, mut jump) = (false,false,false,false,false);

    let pipeline = Arc::new(GraphicsPipeline::start()
        //TODO: use vertex_input with BufferlessDefinition instead
        /*
        .vertex_input(
            BufferlessDefinition.decode(
                BufferlessVertices{
                    vertices: 16,
                    instances: 1,
                }
            )
        )
        // */
        .vertex_input_single_buffer::<iBall>()
        .point_list()
        .vertex_shader(vs.main_entry_point(), ())
        .viewports_dynamic_scissors_irrelevant(1)
        .geometry_shader(gs.main_entry_point(), ())
        .fragment_shader(fs.main_entry_point(), ())
        .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
        .build(device.clone())
        .unwrap());

    let mut framebuffers: Option<Vec<Arc<vulkano::framebuffer::Framebuffer<_,_>>>> = None;

    let mut recreate_swapchain = false;
    
    use vulkano::sync::{
        now,
        GpuFuture,
        FlushError
    };
    let mut previous_frame_end = Box::new(now(device.clone())) as Box<GpuFuture>;
    
    let graphics_set = Arc::new(PersistentDescriptorSet::start(pipeline.clone(), 0)
        .add_buffer(balls.clone()).unwrap()
        .add_buffer(state.clone()).unwrap()
        .build().unwrap());

    loop {
        use std::time;
        let time = time::Instant::now();
        
        //println!{"cleanup"};
        
        previous_frame_end.cleanup_finished();
        
        //println!{"recreate?"};

        if recreate_swapchain {
            println!{"recreating swapchain"}
            dimensions = surface.capabilities(physical)
                        .expect("Getting capabilities failed")
                        .current_extent.unwrap();
            
            let (new_swapchain, new_images) = match swapchain.recreate_with_dimension(dimensions) {
                Ok(r) => r,
                Err(SwapchainCreationError::UnsupportedDimensions) => {
                    eprintln!{"recreation error"}
                    continue;
                },
                Err(err) => panic!("{:?}", err)
            };
            
            mem::replace(&mut swapchain, new_swapchain);
            mem::replace(&mut images, new_images);
            
            framebuffers = None;
            
            recreate_swapchain = false;
            
        }

        //println!{"framebuffers?"};

        use vulkano::framebuffer::Framebuffer;

        if framebuffers.is_none() {
            println!{"new framebuffer"}
            let new_framebuffers = Some(images.iter().map(|image| {
                Arc::new(Framebuffer::start(render_pass.clone())
                             .add(image.clone()).unwrap()
                             .build().unwrap())
            }).collect::<Vec<_>>());
            mem::replace(&mut framebuffers, new_framebuffers);
        }
        
        //println!{"pos_vel"};

        let (image_num, acquire_future) = 
            match acquire_next_image(swapchain.clone(), None) {
                Ok(r) => r,
                Err(AcquireError::OutOfDate) => {
                    println!{"swap out of date"};
                    recreate_swapchain = true;
                    continue;
                }
                Err(err) => panic!("{:?}", err)
            };


        use vulkano::command_buffer::{
            CommandBuffer,
            AutoCommandBufferBuilder,
            DynamicState
        };

        let pos_vel_cmd = AutoCommandBufferBuilder::new(device.clone(), queue.family()).unwrap()
            .dispatch([bsize,1,1], pos_vel_pipe.clone(), pos_vel_set.clone(), ()).unwrap()
            .build().unwrap();
        let finished = pos_vel_cmd.execute(queue.clone()).unwrap();
        
        finished.then_signal_fence_and_flush().unwrap().wait(None).unwrap();

        let collide_cmd = AutoCommandBufferBuilder::new(device.clone(), queue.family()).unwrap()
            .dispatch([bsize,bsize,1], collide_pipe.clone(), collide_set.clone(), ()).unwrap()
            .build().unwrap();

        let finished = collide_cmd.execute(queue.clone()).unwrap();
        
        finished.then_signal_fence_and_flush().unwrap().wait(None).unwrap();

        //println!{"render"};

        use vulkano::pipeline::viewport::{
            Viewport
        };

        let dynamic_state = DynamicState {
            line_width: None,
            viewports: Some(vec![Viewport {
                origin: [0.0, 0.0],
                dimensions:
                //[1.0, 1.0],
                [dimensions[0] as f32, dimensions[1] as f32],
                depth_range: 0.0 .. 1.0,
            }]),
            scissors: None
        };
        
        let command_buffer =
            AutoCommandBufferBuilder::primary_one_time_submit(device.clone(), queue.family())
                .unwrap()
                .begin_render_pass(framebuffers.as_ref().unwrap()[image_num].clone(), false,
                                   vec![[0.0, 0.0, 1.0, 1.0].into()])
                .unwrap()

                .draw(pipeline.clone(), dynamic_state, balls.clone(), graphics_set.clone(), ())
                .unwrap()

                .end_render_pass()
                .unwrap()

                .build()
                .unwrap();

        let maybe_future = previous_frame_end.join(acquire_future)
            .then_execute(queue.clone(), command_buffer).expect("future")

            .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
            .then_signal_fence_and_flush();
        let future = match maybe_future  {
            Ok(future) => future,
            Err(FlushError::OutOfDate) => {
                recreate_swapchain = true;
                previous_frame_end = Box::new(now(device.clone())) as Box<GpuFuture>;
                continue;
            },
            Err(other) => panic!(other)
        };
        
        previous_frame_end = Box::new(future) as Box<_>;
        
        let mut done = false;
        
        events_loop.poll_events(|ev| {
            use winit::{Event, WindowEvent};
            match ev {
                //WindowEvent {event: Closed, ..} => done = true,
                Event::WindowEvent {event: wev, ..} => match wev {
                    WindowEvent::Closed => done = true,
                    WindowEvent::KeyboardInput {
                        input: winit::KeyboardInput {
                            state,
                            virtual_keycode: Some(key),
                        ..},
                    ..} => {
                        use winit::VirtualKeyCode::*;
                        if key!=Escape {
                            println!("{:?}", key);
                        }
                        use winit::ElementState::*;
                        
                        match key {
                            //Add => st.zoom*=0.9,
                            //Subtract => st.zoom/=0.9,
                            Left => left = (state==Pressed),
                            Up => up = (state==Pressed),
                            Right => right = (state==Pressed),
                            Down => down = (state==Pressed),
                            Space => jump = (state==Pressed),
                            Escape => done = true,
                            _ => ()
                        }
                    },
                    WindowEvent::Resized(w, h) => {
                        recreate_swapchain = true
                    },
                    _ => ()
                },
                _ => ()
            }
        });
        
        let mut data = input.write().unwrap();
        
        data.hor = right as i32 - left as i32;
        data.ver = down as i32 - up as i32;
        data.jump = (jump as i32 + 1)*2048;

        
        if done {
            println!("done");
            return;
        }
        
        {
            use std::thread;
            use std::time::Duration;
            
            let fps = 20;

            let wait = Duration::from_millis(1000/fps);

            let elapsed = time.elapsed();

            if wait>elapsed {
                thread::sleep(wait-elapsed);
            } else {
                println!{"Low performance!"};
            }
        }

    }
    
}
